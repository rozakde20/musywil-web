-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2023 at 08:24 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `musywil`
--

-- --------------------------------------------------------

--
-- Table structure for table `pendaftar`
--

CREATE TABLE `pendaftar` (
  `pimpinan` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `nbm` varchar(20) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `daerah` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `tmpt_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nomor` varchar(45) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftar`
--

INSERT INTO `pendaftar` (`pimpinan`, `id`, `nama`, `nik`, `nbm`, `jabatan`, `daerah`, `cabang`, `tmpt_lahir`, `tgl_lahir`, `email`, `nomor`, `users_id`) VALUES
('PWPM JATENG', 11, 'Muzain', '3434555555666666', '100345', 'Sekretaris', '', 'kedungtuban', NULL, NULL, 'zain@gmail.com', '081544125322', 41),
('PDPM', 12, 'Ahmad', '1234666666777890', '3345', 'Kabid', 'Blora', 'kedungtuban', NULL, NULL, 'ahmad@gmail.com', '081332125322', 42),
('PCPM', 13, 'Rosid', '3434987609876789', '4455', 'Bendahara', 'Blora', 'Sale', NULL, NULL, 'rosidi@gmail.com', '083433349987', 44),
('PWPM JATENG', 14, 'Mandra', '3434987644876789', '44121', 'Sekre', 'Blora', 'Sale', NULL, NULL, 'marga@gmail.com', '083433344487', 45),
('PWPM JATENG', 15, 'Hana', '1444455566645678', '1566778', 'anggota', '', '', NULL, NULL, 'hana@gmail.com', '0835656782345', 46),
('PWPM JATENG', 16, 'Marsudi', '5646786543345678', '7666555', 'Anggota', '', '', 'Semarang', '1984-11-28', 'marsudi@gmail.com', '081567659987', 48);

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `foto_ktp` varchar(100) DEFAULT NULL,
  `foto_nbm` varchar(100) DEFAULT NULL,
  `pas_foto` varchar(100) DEFAULT NULL,
  `surat_mandat` varchar(100) DEFAULT NULL,
  `sk_pimpinan` varchar(100) DEFAULT NULL,
  `swo` varchar(100) DEFAULT NULL,
  `swp` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `pendaftar_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`, `level`) VALUES
(41, 'Muzain', 'zain@gmail.com', '1234', 'peserta'),
(42, 'Ahmad', 'ahmad@gmail.com', '1234', 'peserta'),
(44, 'Rosid', 'rosidi@gmail.com', '1234', 'peserta'),
(45, 'Mandra', 'marga@gmail.com', '1234', 'peserta'),
(46, 'Hana', 'hana@gmail.com', '1234', 'peserta'),
(47, 'administrator', 'admin@gmail.com', '12345678', 'admin'),
(48, 'Marsudi', 'marsudi@gmail.com', '1234', 'peserta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_users_id` (`users_id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_nilai_pendaftar1_idx` (`pendaftar_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pendaftar`
--
ALTER TABLE `pendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
