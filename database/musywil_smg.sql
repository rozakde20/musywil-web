/*
 Navicat Premium Data Transfer

 Source Server         : a
 Source Server Type    : MySQL
 Source Server Version : 100428
 Source Host           : localhost:3306
 Source Schema         : musywil

 Target Server Type    : MySQL
 Target Server Version : 100428
 File Encoding         : 65001

 Date: 16/06/2023 15:21:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pendaftar
-- ----------------------------
DROP TABLE IF EXISTS `pendaftar`;
CREATE TABLE `pendaftar`  (
  `pimpinan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nik` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nbm` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `daerah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cabang` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tmpt_lahir` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kaos` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_daftar` datetime(0) NULL DEFAULT NULL,
  `users_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  INDEX `fk_users_id`(`users_id`) USING BTREE,
  CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pendaftar
-- ----------------------------
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 11, 'Muzain', '3434555555666666', '100345', 'Sekretaris', '', 'kedungtuban', NULL, NULL, 'zain@gmail.com', '081544125322', NULL, NULL, 41);
INSERT INTO `pendaftar` VALUES ('PDPM', 12, 'Ahmad', '1234666666777890', '3345', 'Kabid', 'Blora', 'kedungtuban', NULL, NULL, 'ahmad@gmail.com', '081332125322', NULL, NULL, 42);
INSERT INTO `pendaftar` VALUES ('PCPM', 13, 'Rosid', '3434987609876789', '4455', 'Bendahara', 'Blora', 'Sale', NULL, NULL, 'rosidi@gmail.com', '083433349987', NULL, NULL, 44);
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 14, 'Mandra', '3434987644876789', '44121', 'Sekre', 'Blora', 'Sale', NULL, NULL, 'marga@gmail.com', '083433344487', NULL, NULL, 45);
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 15, 'Hana', '1444455566645678', '1566778', 'anggota', '', '', NULL, NULL, 'hana@gmail.com', '0835656782345', NULL, NULL, 46);
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 16, 'Marsudi', '5646786543345678', '7666555', 'Anggota', '', '', 'Semarang', '1984-11-28', 'marsudi@gmail.com', '081567659987', NULL, NULL, 48);
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 27, 'Dr. Muhammad Muza\'in, S.Kom, M.Kom', '3374070381102700', '110737171261181', 'Ketua', '3374', '337407', 'Semarang', '1981-10-27', 'admin@admin.com', '6285123456789', 'XL', '2023-06-16 10:05:53', NULL);
INSERT INTO `pendaftar` VALUES ('PWPM JATENG', 28, 'SMG', '1245678979', '111111111111111', 'Direktur', '3326', '332613', 'Pekalongan', '1995-08-17', 'owner@netnusantara.id', '62851234567445', 'XL', '2023-06-16 10:01:16', NULL);

-- ----------------------------
-- Table structure for tbl_cabang
-- ----------------------------
DROP TABLE IF EXISTS `tbl_cabang`;
CREATE TABLE `tbl_cabang`  (
  `kode_cabang` int(6) NOT NULL,
  `nama_cabang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`kode_cabang`) USING BTREE,
  UNIQUE INDEX `unix`(`kode_cabang`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_cabang
-- ----------------------------
INSERT INTO `tbl_cabang` VALUES (330101, ' Kedungreja');
INSERT INTO `tbl_cabang` VALUES (330102, ' Kesugihan');
INSERT INTO `tbl_cabang` VALUES (330103, ' Adipala');
INSERT INTO `tbl_cabang` VALUES (330104, ' Binangun');
INSERT INTO `tbl_cabang` VALUES (330105, ' Nusawungu');
INSERT INTO `tbl_cabang` VALUES (330106, ' Kroya');
INSERT INTO `tbl_cabang` VALUES (330107, ' Maos');
INSERT INTO `tbl_cabang` VALUES (330108, ' Jeruklegi');
INSERT INTO `tbl_cabang` VALUES (330109, ' Kawunganten');
INSERT INTO `tbl_cabang` VALUES (330110, ' Gandrungmangu');
INSERT INTO `tbl_cabang` VALUES (330111, ' Sidareja');
INSERT INTO `tbl_cabang` VALUES (330112, ' Karangpucung');
INSERT INTO `tbl_cabang` VALUES (330113, ' Cimanggu');
INSERT INTO `tbl_cabang` VALUES (330114, ' Majenang');
INSERT INTO `tbl_cabang` VALUES (330115, ' Wanareja');
INSERT INTO `tbl_cabang` VALUES (330116, ' Dayeuhluhur');
INSERT INTO `tbl_cabang` VALUES (330117, ' Sampang');
INSERT INTO `tbl_cabang` VALUES (330118, ' Cipari');
INSERT INTO `tbl_cabang` VALUES (330119, ' Patimuan');
INSERT INTO `tbl_cabang` VALUES (330120, ' Bantarsari');
INSERT INTO `tbl_cabang` VALUES (330121, ' Cilacap Selatan');
INSERT INTO `tbl_cabang` VALUES (330122, ' Cilacap Tengah');
INSERT INTO `tbl_cabang` VALUES (330123, ' Cilacap Utara');
INSERT INTO `tbl_cabang` VALUES (330124, ' Kampung Laut');
INSERT INTO `tbl_cabang` VALUES (330201, ' Lumbir');
INSERT INTO `tbl_cabang` VALUES (330202, ' Wangon');
INSERT INTO `tbl_cabang` VALUES (330203, ' Jatilawang');
INSERT INTO `tbl_cabang` VALUES (330204, ' Rawalo');
INSERT INTO `tbl_cabang` VALUES (330205, ' Kebasen');
INSERT INTO `tbl_cabang` VALUES (330206, ' Kemranjen');
INSERT INTO `tbl_cabang` VALUES (330207, ' Sumpiuh');
INSERT INTO `tbl_cabang` VALUES (330208, ' Tambak');
INSERT INTO `tbl_cabang` VALUES (330209, ' Somagede');
INSERT INTO `tbl_cabang` VALUES (330210, ' Kalibagor');
INSERT INTO `tbl_cabang` VALUES (330211, ' Banyumas');
INSERT INTO `tbl_cabang` VALUES (330212, ' Patikraja');
INSERT INTO `tbl_cabang` VALUES (330213, ' Purwojati');
INSERT INTO `tbl_cabang` VALUES (330214, ' Ajibarang');
INSERT INTO `tbl_cabang` VALUES (330215, ' Gumelar');
INSERT INTO `tbl_cabang` VALUES (330216, ' Pekuncen');
INSERT INTO `tbl_cabang` VALUES (330217, ' Cilongok');
INSERT INTO `tbl_cabang` VALUES (330218, ' Karanglewas');
INSERT INTO `tbl_cabang` VALUES (330219, ' Sokaraja');
INSERT INTO `tbl_cabang` VALUES (330220, ' Kembaran');
INSERT INTO `tbl_cabang` VALUES (330221, ' Sumbang');
INSERT INTO `tbl_cabang` VALUES (330222, ' Baturraden');
INSERT INTO `tbl_cabang` VALUES (330223, ' Kedungbanteng');
INSERT INTO `tbl_cabang` VALUES (330224, ' Purwokerto Selatan');
INSERT INTO `tbl_cabang` VALUES (330225, ' Purwokerto Barat');
INSERT INTO `tbl_cabang` VALUES (330226, ' Purwokerto Timur');
INSERT INTO `tbl_cabang` VALUES (330227, ' Purwokerto Utara');
INSERT INTO `tbl_cabang` VALUES (330301, ' Kemangkon');
INSERT INTO `tbl_cabang` VALUES (330302, ' Bukateja');
INSERT INTO `tbl_cabang` VALUES (330303, ' Kejobong');
INSERT INTO `tbl_cabang` VALUES (330304, ' Kaligondang');
INSERT INTO `tbl_cabang` VALUES (330305, ' Purbalingga');
INSERT INTO `tbl_cabang` VALUES (330306, ' Kalimanah');
INSERT INTO `tbl_cabang` VALUES (330307, ' Kutasari');
INSERT INTO `tbl_cabang` VALUES (330308, ' Mrebet');
INSERT INTO `tbl_cabang` VALUES (330309, ' Bobotsari');
INSERT INTO `tbl_cabang` VALUES (330310, ' Karangreja');
INSERT INTO `tbl_cabang` VALUES (330311, ' Karanganyar');
INSERT INTO `tbl_cabang` VALUES (330312, ' Karangmoncol');
INSERT INTO `tbl_cabang` VALUES (330313, ' Rembang');
INSERT INTO `tbl_cabang` VALUES (330314, ' Bojongsari');
INSERT INTO `tbl_cabang` VALUES (330315, ' Padamara');
INSERT INTO `tbl_cabang` VALUES (330316, ' Pengadegan');
INSERT INTO `tbl_cabang` VALUES (330317, ' Karangjambu');
INSERT INTO `tbl_cabang` VALUES (330318, ' Kertanegara');
INSERT INTO `tbl_cabang` VALUES (330401, ' Susukan');
INSERT INTO `tbl_cabang` VALUES (330402, ' Purworeja Klampok');
INSERT INTO `tbl_cabang` VALUES (330403, ' Mandiraja');
INSERT INTO `tbl_cabang` VALUES (330404, ' Purwanegara');
INSERT INTO `tbl_cabang` VALUES (330405, ' Bawang');
INSERT INTO `tbl_cabang` VALUES (330406, ' Banjarnegara');
INSERT INTO `tbl_cabang` VALUES (330407, ' Sigaluh');
INSERT INTO `tbl_cabang` VALUES (330408, ' Madukara');
INSERT INTO `tbl_cabang` VALUES (330409, ' Banjarmangu');
INSERT INTO `tbl_cabang` VALUES (330410, ' Wanadadi');
INSERT INTO `tbl_cabang` VALUES (330411, ' Rakit');
INSERT INTO `tbl_cabang` VALUES (330412, ' Punggelan');
INSERT INTO `tbl_cabang` VALUES (330413, ' Karangkobar');
INSERT INTO `tbl_cabang` VALUES (330414, ' Pagentan');
INSERT INTO `tbl_cabang` VALUES (330415, ' Pejawaran');
INSERT INTO `tbl_cabang` VALUES (330416, ' Batur');
INSERT INTO `tbl_cabang` VALUES (330417, ' Wanayasa');
INSERT INTO `tbl_cabang` VALUES (330418, ' Kalibening');
INSERT INTO `tbl_cabang` VALUES (330419, ' Pandanarum');
INSERT INTO `tbl_cabang` VALUES (330420, ' Pagedongan');
INSERT INTO `tbl_cabang` VALUES (330501, ' Ayah');
INSERT INTO `tbl_cabang` VALUES (330502, ' Buayan');
INSERT INTO `tbl_cabang` VALUES (330503, ' Puring');
INSERT INTO `tbl_cabang` VALUES (330504, ' Petanahan');
INSERT INTO `tbl_cabang` VALUES (330505, ' Klirong');
INSERT INTO `tbl_cabang` VALUES (330506, ' Buluspesantren');
INSERT INTO `tbl_cabang` VALUES (330507, ' Ambal');
INSERT INTO `tbl_cabang` VALUES (330508, ' Mirit');
INSERT INTO `tbl_cabang` VALUES (330509, ' Prembun');
INSERT INTO `tbl_cabang` VALUES (330510, ' Kutowinangun');
INSERT INTO `tbl_cabang` VALUES (330511, ' Alian');
INSERT INTO `tbl_cabang` VALUES (330512, ' Kebumen');
INSERT INTO `tbl_cabang` VALUES (330513, ' Pejagoan');
INSERT INTO `tbl_cabang` VALUES (330514, ' Sruweng');
INSERT INTO `tbl_cabang` VALUES (330515, ' Adimulyo');
INSERT INTO `tbl_cabang` VALUES (330516, ' Kuwarasan');
INSERT INTO `tbl_cabang` VALUES (330517, ' Rowokele');
INSERT INTO `tbl_cabang` VALUES (330518, ' Sempor');
INSERT INTO `tbl_cabang` VALUES (330519, ' Gombong');
INSERT INTO `tbl_cabang` VALUES (330520, ' Karanganyar');
INSERT INTO `tbl_cabang` VALUES (330521, ' Karanggayam');
INSERT INTO `tbl_cabang` VALUES (330522, ' Sadang');
INSERT INTO `tbl_cabang` VALUES (330523, ' Bonorowo');
INSERT INTO `tbl_cabang` VALUES (330524, ' Padureso');
INSERT INTO `tbl_cabang` VALUES (330525, ' Poncowarno');
INSERT INTO `tbl_cabang` VALUES (330526, ' Karangsambung');
INSERT INTO `tbl_cabang` VALUES (330601, ' Grabag');
INSERT INTO `tbl_cabang` VALUES (330602, ' Ngombol');
INSERT INTO `tbl_cabang` VALUES (330603, ' Purwodadi');
INSERT INTO `tbl_cabang` VALUES (330604, ' Bagelen');
INSERT INTO `tbl_cabang` VALUES (330605, ' Kaligesing');
INSERT INTO `tbl_cabang` VALUES (330606, ' Purworejo');
INSERT INTO `tbl_cabang` VALUES (330607, ' Banyuurip');
INSERT INTO `tbl_cabang` VALUES (330608, ' Bayan');
INSERT INTO `tbl_cabang` VALUES (330609, ' Kutoarjo');
INSERT INTO `tbl_cabang` VALUES (330610, ' Butuh');
INSERT INTO `tbl_cabang` VALUES (330611, ' Pituruh');
INSERT INTO `tbl_cabang` VALUES (330612, ' Kemiri');
INSERT INTO `tbl_cabang` VALUES (330613, ' Bruno');
INSERT INTO `tbl_cabang` VALUES (330614, ' Gebang');
INSERT INTO `tbl_cabang` VALUES (330615, ' Loano');
INSERT INTO `tbl_cabang` VALUES (330616, ' Bener');
INSERT INTO `tbl_cabang` VALUES (330701, ' Wadaslintang');
INSERT INTO `tbl_cabang` VALUES (330702, ' Kepil');
INSERT INTO `tbl_cabang` VALUES (330703, ' Sapuran');
INSERT INTO `tbl_cabang` VALUES (330704, ' Kaliwiro');
INSERT INTO `tbl_cabang` VALUES (330705, ' Leksono');
INSERT INTO `tbl_cabang` VALUES (330706, ' Selomerto');
INSERT INTO `tbl_cabang` VALUES (330707, ' Kalikajar');
INSERT INTO `tbl_cabang` VALUES (330708, ' Kertek');
INSERT INTO `tbl_cabang` VALUES (330709, ' Wonosobo');
INSERT INTO `tbl_cabang` VALUES (330710, ' Watumalang');
INSERT INTO `tbl_cabang` VALUES (330711, ' Mojotengah');
INSERT INTO `tbl_cabang` VALUES (330712, ' Garung');
INSERT INTO `tbl_cabang` VALUES (330713, ' Kejajar');
INSERT INTO `tbl_cabang` VALUES (330714, ' Sukoharjo');
INSERT INTO `tbl_cabang` VALUES (330715, ' Kalibawang');
INSERT INTO `tbl_cabang` VALUES (330801, ' Salaman');
INSERT INTO `tbl_cabang` VALUES (330802, ' Borobudur');
INSERT INTO `tbl_cabang` VALUES (330803, ' Ngluwar');
INSERT INTO `tbl_cabang` VALUES (330804, ' Salam');
INSERT INTO `tbl_cabang` VALUES (330805, ' Srumbung');
INSERT INTO `tbl_cabang` VALUES (330806, ' Dukun');
INSERT INTO `tbl_cabang` VALUES (330807, ' Sawangan');
INSERT INTO `tbl_cabang` VALUES (330808, ' Muntilan');
INSERT INTO `tbl_cabang` VALUES (330809, ' Mungkid');
INSERT INTO `tbl_cabang` VALUES (330810, ' Mertoyudan');
INSERT INTO `tbl_cabang` VALUES (330811, ' Tempuran');
INSERT INTO `tbl_cabang` VALUES (330812, ' Kajoran');
INSERT INTO `tbl_cabang` VALUES (330813, ' Kaliangkrik');
INSERT INTO `tbl_cabang` VALUES (330814, ' Bandongan');
INSERT INTO `tbl_cabang` VALUES (330815, ' Candimulyo');
INSERT INTO `tbl_cabang` VALUES (330816, ' Pakis');
INSERT INTO `tbl_cabang` VALUES (330817, ' Ngablak');
INSERT INTO `tbl_cabang` VALUES (330818, ' Grabag');
INSERT INTO `tbl_cabang` VALUES (330819, ' Tegalrejo');
INSERT INTO `tbl_cabang` VALUES (330820, ' Secang');
INSERT INTO `tbl_cabang` VALUES (330821, ' Windusari');
INSERT INTO `tbl_cabang` VALUES (330901, ' Selo');
INSERT INTO `tbl_cabang` VALUES (330902, ' Ampel');
INSERT INTO `tbl_cabang` VALUES (330903, ' Cepogo');
INSERT INTO `tbl_cabang` VALUES (330904, ' Musuk');
INSERT INTO `tbl_cabang` VALUES (330905, ' Boyolali');
INSERT INTO `tbl_cabang` VALUES (330906, ' Mojosongo');
INSERT INTO `tbl_cabang` VALUES (330907, ' Teras');
INSERT INTO `tbl_cabang` VALUES (330908, ' Sawit');
INSERT INTO `tbl_cabang` VALUES (330909, ' Banyudono');
INSERT INTO `tbl_cabang` VALUES (330910, ' Sambi');
INSERT INTO `tbl_cabang` VALUES (330911, ' Ngemplak');
INSERT INTO `tbl_cabang` VALUES (330912, ' Nogosari');
INSERT INTO `tbl_cabang` VALUES (330913, ' Simo');
INSERT INTO `tbl_cabang` VALUES (330914, ' Karanggede');
INSERT INTO `tbl_cabang` VALUES (330915, ' Klego');
INSERT INTO `tbl_cabang` VALUES (330916, ' Andong');
INSERT INTO `tbl_cabang` VALUES (330917, ' Kemusu');
INSERT INTO `tbl_cabang` VALUES (330918, ' Wonosegoro');
INSERT INTO `tbl_cabang` VALUES (330919, ' Juwangi');
INSERT INTO `tbl_cabang` VALUES (330920, ' Gladagsari');
INSERT INTO `tbl_cabang` VALUES (330921, ' Tamansari');
INSERT INTO `tbl_cabang` VALUES (330922, ' Wonosamodro');
INSERT INTO `tbl_cabang` VALUES (331001, ' Prambanan');
INSERT INTO `tbl_cabang` VALUES (331002, ' Gantiwarno');
INSERT INTO `tbl_cabang` VALUES (331003, ' Wedi');
INSERT INTO `tbl_cabang` VALUES (331004, ' Bayat');
INSERT INTO `tbl_cabang` VALUES (331005, ' Cawas');
INSERT INTO `tbl_cabang` VALUES (331006, ' Trucuk');
INSERT INTO `tbl_cabang` VALUES (331007, ' Kebonarum');
INSERT INTO `tbl_cabang` VALUES (331008, ' Jogonalan');
INSERT INTO `tbl_cabang` VALUES (331009, ' Manisrenggo');
INSERT INTO `tbl_cabang` VALUES (331010, ' Karangnongko');
INSERT INTO `tbl_cabang` VALUES (331011, ' Ceper');
INSERT INTO `tbl_cabang` VALUES (331012, ' Pedan');
INSERT INTO `tbl_cabang` VALUES (331013, ' Karangdowo');
INSERT INTO `tbl_cabang` VALUES (331014, ' Juwiring');
INSERT INTO `tbl_cabang` VALUES (331015, ' Wonosari');
INSERT INTO `tbl_cabang` VALUES (331016, ' Delanggu');
INSERT INTO `tbl_cabang` VALUES (331017, ' Polanharjo');
INSERT INTO `tbl_cabang` VALUES (331018, ' Karanganom');
INSERT INTO `tbl_cabang` VALUES (331019, ' Tulung');
INSERT INTO `tbl_cabang` VALUES (331020, ' Jatinom');
INSERT INTO `tbl_cabang` VALUES (331021, ' Kemalang');
INSERT INTO `tbl_cabang` VALUES (331022, ' Ngawen');
INSERT INTO `tbl_cabang` VALUES (331023, ' Kalikotes');
INSERT INTO `tbl_cabang` VALUES (331024, ' Klaten Utara');
INSERT INTO `tbl_cabang` VALUES (331025, ' Klaten Tengah');
INSERT INTO `tbl_cabang` VALUES (331026, ' Klaten Selatan');
INSERT INTO `tbl_cabang` VALUES (331101, ' Weru');
INSERT INTO `tbl_cabang` VALUES (331102, ' Bulu');
INSERT INTO `tbl_cabang` VALUES (331103, ' Tawangsari');
INSERT INTO `tbl_cabang` VALUES (331104, ' Sukoharjo');
INSERT INTO `tbl_cabang` VALUES (331105, ' Nguter');
INSERT INTO `tbl_cabang` VALUES (331106, ' Bendosari');
INSERT INTO `tbl_cabang` VALUES (331107, ' Polokarto');
INSERT INTO `tbl_cabang` VALUES (331108, ' Mojolaban');
INSERT INTO `tbl_cabang` VALUES (331109, ' Grogol');
INSERT INTO `tbl_cabang` VALUES (331110, ' Baki');
INSERT INTO `tbl_cabang` VALUES (331111, ' Gatak');
INSERT INTO `tbl_cabang` VALUES (331112, ' Kartasura');
INSERT INTO `tbl_cabang` VALUES (331201, ' Pracimantoro');
INSERT INTO `tbl_cabang` VALUES (331202, ' Giritontro');
INSERT INTO `tbl_cabang` VALUES (331203, ' Giriwoyo');
INSERT INTO `tbl_cabang` VALUES (331204, ' Batuwarno');
INSERT INTO `tbl_cabang` VALUES (331205, ' Tirtomoyo');
INSERT INTO `tbl_cabang` VALUES (331206, ' Nguntoronadi');
INSERT INTO `tbl_cabang` VALUES (331207, ' Baturetno');
INSERT INTO `tbl_cabang` VALUES (331208, ' Eromoko');
INSERT INTO `tbl_cabang` VALUES (331209, ' Wuryantoro');
INSERT INTO `tbl_cabang` VALUES (331210, ' Manyaran');
INSERT INTO `tbl_cabang` VALUES (331211, ' Selogiri');
INSERT INTO `tbl_cabang` VALUES (331212, ' Wonogiri');
INSERT INTO `tbl_cabang` VALUES (331213, ' Ngadirojo');
INSERT INTO `tbl_cabang` VALUES (331214, ' Sidoharjo');
INSERT INTO `tbl_cabang` VALUES (331215, ' Jatiroto');
INSERT INTO `tbl_cabang` VALUES (331216, ' Kismantoro');
INSERT INTO `tbl_cabang` VALUES (331217, ' Purwantoro');
INSERT INTO `tbl_cabang` VALUES (331218, ' Bulukerto');
INSERT INTO `tbl_cabang` VALUES (331219, ' Slogohimo');
INSERT INTO `tbl_cabang` VALUES (331220, ' Jatisrono');
INSERT INTO `tbl_cabang` VALUES (331221, ' Jatipurno');
INSERT INTO `tbl_cabang` VALUES (331222, ' Girimarto');
INSERT INTO `tbl_cabang` VALUES (331223, ' Karangtengah');
INSERT INTO `tbl_cabang` VALUES (331224, ' Paranggupito');
INSERT INTO `tbl_cabang` VALUES (331225, ' Puhpelem');
INSERT INTO `tbl_cabang` VALUES (331301, ' Jatipuro');
INSERT INTO `tbl_cabang` VALUES (331302, ' Jatiyoso');
INSERT INTO `tbl_cabang` VALUES (331303, ' Jumapolo');
INSERT INTO `tbl_cabang` VALUES (331304, ' Jumantono');
INSERT INTO `tbl_cabang` VALUES (331305, ' Matesih');
INSERT INTO `tbl_cabang` VALUES (331306, ' Tawangmangu');
INSERT INTO `tbl_cabang` VALUES (331307, ' Ngargoyoso');
INSERT INTO `tbl_cabang` VALUES (331308, ' Karangpandan');
INSERT INTO `tbl_cabang` VALUES (331309, ' Karanganyar');
INSERT INTO `tbl_cabang` VALUES (331310, ' Tasikmadu');
INSERT INTO `tbl_cabang` VALUES (331311, ' Jaten');
INSERT INTO `tbl_cabang` VALUES (331312, ' Colomadu');
INSERT INTO `tbl_cabang` VALUES (331313, ' Gondangrejo');
INSERT INTO `tbl_cabang` VALUES (331314, ' Kebakkramat');
INSERT INTO `tbl_cabang` VALUES (331315, ' Mojogedang');
INSERT INTO `tbl_cabang` VALUES (331316, ' Kerjo');
INSERT INTO `tbl_cabang` VALUES (331317, ' Jenawi');
INSERT INTO `tbl_cabang` VALUES (331401, ' Kalijambe');
INSERT INTO `tbl_cabang` VALUES (331402, ' Plupuh');
INSERT INTO `tbl_cabang` VALUES (331403, ' Masaran');
INSERT INTO `tbl_cabang` VALUES (331404, ' Kedawung');
INSERT INTO `tbl_cabang` VALUES (331405, ' Sambirejo');
INSERT INTO `tbl_cabang` VALUES (331406, ' Gondang');
INSERT INTO `tbl_cabang` VALUES (331407, ' Sambungmacan');
INSERT INTO `tbl_cabang` VALUES (331408, ' Ngrampal');
INSERT INTO `tbl_cabang` VALUES (331409, ' Karangmalang');
INSERT INTO `tbl_cabang` VALUES (331410, ' Sragen');
INSERT INTO `tbl_cabang` VALUES (331411, ' Sidoharjo');
INSERT INTO `tbl_cabang` VALUES (331412, ' Tanon');
INSERT INTO `tbl_cabang` VALUES (331413, ' Gemolong');
INSERT INTO `tbl_cabang` VALUES (331414, ' Miri');
INSERT INTO `tbl_cabang` VALUES (331415, ' Sumberlawang');
INSERT INTO `tbl_cabang` VALUES (331416, ' Mondokan');
INSERT INTO `tbl_cabang` VALUES (331417, ' Sukodono');
INSERT INTO `tbl_cabang` VALUES (331418, ' Gesi');
INSERT INTO `tbl_cabang` VALUES (331419, ' Tangen');
INSERT INTO `tbl_cabang` VALUES (331420, ' Jenar');
INSERT INTO `tbl_cabang` VALUES (331501, ' Kedungjati');
INSERT INTO `tbl_cabang` VALUES (331502, ' Karangrayung');
INSERT INTO `tbl_cabang` VALUES (331503, ' Penawangan');
INSERT INTO `tbl_cabang` VALUES (331504, ' Toroh');
INSERT INTO `tbl_cabang` VALUES (331505, ' Geyer');
INSERT INTO `tbl_cabang` VALUES (331506, ' Pulokulon');
INSERT INTO `tbl_cabang` VALUES (331507, ' Kradenan');
INSERT INTO `tbl_cabang` VALUES (331508, ' Gabus');
INSERT INTO `tbl_cabang` VALUES (331509, ' Ngaringan');
INSERT INTO `tbl_cabang` VALUES (331510, ' Wirosari');
INSERT INTO `tbl_cabang` VALUES (331511, ' Tawangharjo');
INSERT INTO `tbl_cabang` VALUES (331512, ' Grobogan');
INSERT INTO `tbl_cabang` VALUES (331513, ' Purwodadi');
INSERT INTO `tbl_cabang` VALUES (331514, ' Brati');
INSERT INTO `tbl_cabang` VALUES (331515, ' Klambu');
INSERT INTO `tbl_cabang` VALUES (331516, ' Godong');
INSERT INTO `tbl_cabang` VALUES (331517, ' Gubug');
INSERT INTO `tbl_cabang` VALUES (331518, ' Tegowanu');
INSERT INTO `tbl_cabang` VALUES (331519, ' Tanggungharjo');
INSERT INTO `tbl_cabang` VALUES (331601, ' Jati');
INSERT INTO `tbl_cabang` VALUES (331602, ' Randublatung');
INSERT INTO `tbl_cabang` VALUES (331603, ' Kradenan');
INSERT INTO `tbl_cabang` VALUES (331604, ' Kedungtuban');
INSERT INTO `tbl_cabang` VALUES (331605, ' Cepu');
INSERT INTO `tbl_cabang` VALUES (331606, ' Sambong');
INSERT INTO `tbl_cabang` VALUES (331607, ' Jiken');
INSERT INTO `tbl_cabang` VALUES (331608, ' Jepon');
INSERT INTO `tbl_cabang` VALUES (331609, ' Blora');
INSERT INTO `tbl_cabang` VALUES (331610, ' Tunjungan');
INSERT INTO `tbl_cabang` VALUES (331611, ' Banjarejo');
INSERT INTO `tbl_cabang` VALUES (331612, ' Ngawen');
INSERT INTO `tbl_cabang` VALUES (331613, ' Kunduran');
INSERT INTO `tbl_cabang` VALUES (331614, ' Todanan');
INSERT INTO `tbl_cabang` VALUES (331615, ' Bogorejo');
INSERT INTO `tbl_cabang` VALUES (331616, ' Japah');
INSERT INTO `tbl_cabang` VALUES (331701, ' Sumber');
INSERT INTO `tbl_cabang` VALUES (331702, ' Bulu');
INSERT INTO `tbl_cabang` VALUES (331703, ' Gunem');
INSERT INTO `tbl_cabang` VALUES (331704, ' Sale');
INSERT INTO `tbl_cabang` VALUES (331705, ' Sarang');
INSERT INTO `tbl_cabang` VALUES (331706, ' Sedan');
INSERT INTO `tbl_cabang` VALUES (331707, ' Pamotan');
INSERT INTO `tbl_cabang` VALUES (331708, ' Sulang');
INSERT INTO `tbl_cabang` VALUES (331709, ' Kaliori');
INSERT INTO `tbl_cabang` VALUES (331710, ' Rembang');
INSERT INTO `tbl_cabang` VALUES (331711, ' Pancur');
INSERT INTO `tbl_cabang` VALUES (331712, ' Kragan');
INSERT INTO `tbl_cabang` VALUES (331713, ' Sluke');
INSERT INTO `tbl_cabang` VALUES (331714, ' Lasem');
INSERT INTO `tbl_cabang` VALUES (331801, ' Sukolilo');
INSERT INTO `tbl_cabang` VALUES (331802, ' Kayen');
INSERT INTO `tbl_cabang` VALUES (331803, ' Tambakromo');
INSERT INTO `tbl_cabang` VALUES (331804, ' Winong');
INSERT INTO `tbl_cabang` VALUES (331805, ' Pucakwangi');
INSERT INTO `tbl_cabang` VALUES (331806, ' Jaken');
INSERT INTO `tbl_cabang` VALUES (331807, ' Batangan');
INSERT INTO `tbl_cabang` VALUES (331808, ' Juwana');
INSERT INTO `tbl_cabang` VALUES (331809, ' Jakenan');
INSERT INTO `tbl_cabang` VALUES (331810, ' Pati');
INSERT INTO `tbl_cabang` VALUES (331811, ' Gabus');
INSERT INTO `tbl_cabang` VALUES (331812, ' Margorejo');
INSERT INTO `tbl_cabang` VALUES (331813, ' Gembong');
INSERT INTO `tbl_cabang` VALUES (331814, ' Tlogowungu');
INSERT INTO `tbl_cabang` VALUES (331815, ' Wedarijaksa');
INSERT INTO `tbl_cabang` VALUES (331816, ' Margoyoso');
INSERT INTO `tbl_cabang` VALUES (331817, ' Gunungwungkal');
INSERT INTO `tbl_cabang` VALUES (331818, ' Cluwak');
INSERT INTO `tbl_cabang` VALUES (331819, ' Tayu');
INSERT INTO `tbl_cabang` VALUES (331820, ' Dukuhseti');
INSERT INTO `tbl_cabang` VALUES (331821, ' Trangkil');
INSERT INTO `tbl_cabang` VALUES (331901, ' Kaliwungu');
INSERT INTO `tbl_cabang` VALUES (331902, ' Kota Kudus');
INSERT INTO `tbl_cabang` VALUES (331903, ' Jati');
INSERT INTO `tbl_cabang` VALUES (331904, ' Undaan');
INSERT INTO `tbl_cabang` VALUES (331905, ' Mejobo');
INSERT INTO `tbl_cabang` VALUES (331906, ' Jekulo');
INSERT INTO `tbl_cabang` VALUES (331907, ' Bae');
INSERT INTO `tbl_cabang` VALUES (331908, ' Gebog');
INSERT INTO `tbl_cabang` VALUES (331909, ' Dawe');
INSERT INTO `tbl_cabang` VALUES (332001, ' Kedung');
INSERT INTO `tbl_cabang` VALUES (332002, ' Pecangaan');
INSERT INTO `tbl_cabang` VALUES (332003, ' Welahan');
INSERT INTO `tbl_cabang` VALUES (332004, ' Mayong');
INSERT INTO `tbl_cabang` VALUES (332005, ' Batealit');
INSERT INTO `tbl_cabang` VALUES (332006, ' Jepara');
INSERT INTO `tbl_cabang` VALUES (332007, ' Mlonggo');
INSERT INTO `tbl_cabang` VALUES (332008, ' Bangsri');
INSERT INTO `tbl_cabang` VALUES (332009, ' Keling');
INSERT INTO `tbl_cabang` VALUES (332010, ' Karimun Jawa');
INSERT INTO `tbl_cabang` VALUES (332011, ' Tahunan');
INSERT INTO `tbl_cabang` VALUES (332012, ' Nalumsari');
INSERT INTO `tbl_cabang` VALUES (332013, ' Kalinyamatan');
INSERT INTO `tbl_cabang` VALUES (332014, ' Kembang');
INSERT INTO `tbl_cabang` VALUES (332015, ' Pakis Aji');
INSERT INTO `tbl_cabang` VALUES (332016, ' Donorojo');
INSERT INTO `tbl_cabang` VALUES (332101, ' Mranggen');
INSERT INTO `tbl_cabang` VALUES (332102, ' Karangawen');
INSERT INTO `tbl_cabang` VALUES (332103, ' Guntur');
INSERT INTO `tbl_cabang` VALUES (332104, ' Sayung');
INSERT INTO `tbl_cabang` VALUES (332105, ' Karangtengah');
INSERT INTO `tbl_cabang` VALUES (332106, ' Wonosalam');
INSERT INTO `tbl_cabang` VALUES (332107, ' Dempet');
INSERT INTO `tbl_cabang` VALUES (332108, ' Gajah');
INSERT INTO `tbl_cabang` VALUES (332109, ' Karanganyar');
INSERT INTO `tbl_cabang` VALUES (332110, ' Mijen');
INSERT INTO `tbl_cabang` VALUES (332111, ' Demak');
INSERT INTO `tbl_cabang` VALUES (332112, ' Bonang');
INSERT INTO `tbl_cabang` VALUES (332113, ' Wedung');
INSERT INTO `tbl_cabang` VALUES (332114, ' Kebonagung');
INSERT INTO `tbl_cabang` VALUES (332201, ' Getasan');
INSERT INTO `tbl_cabang` VALUES (332202, ' Tengaran');
INSERT INTO `tbl_cabang` VALUES (332203, ' Susukan');
INSERT INTO `tbl_cabang` VALUES (332204, ' Suruh');
INSERT INTO `tbl_cabang` VALUES (332205, ' Pabelan');
INSERT INTO `tbl_cabang` VALUES (332206, ' Tuntang');
INSERT INTO `tbl_cabang` VALUES (332207, ' Banyubiru');
INSERT INTO `tbl_cabang` VALUES (332208, ' Jambu');
INSERT INTO `tbl_cabang` VALUES (332209, ' Sumowono');
INSERT INTO `tbl_cabang` VALUES (332210, ' Ambarawa');
INSERT INTO `tbl_cabang` VALUES (332211, ' Bawen');
INSERT INTO `tbl_cabang` VALUES (332212, ' Bringin');
INSERT INTO `tbl_cabang` VALUES (332213, ' Bergas');
INSERT INTO `tbl_cabang` VALUES (332215, ' Pringapus');
INSERT INTO `tbl_cabang` VALUES (332216, ' Bancak');
INSERT INTO `tbl_cabang` VALUES (332217, ' Kaliwungu');
INSERT INTO `tbl_cabang` VALUES (332218, ' Ungaran Barat');
INSERT INTO `tbl_cabang` VALUES (332219, ' Ungaran Timur');
INSERT INTO `tbl_cabang` VALUES (332220, ' Bandungan');
INSERT INTO `tbl_cabang` VALUES (332301, ' Bulu');
INSERT INTO `tbl_cabang` VALUES (332302, ' Tembarak');
INSERT INTO `tbl_cabang` VALUES (332303, ' Temanggung');
INSERT INTO `tbl_cabang` VALUES (332304, ' Pringsurat');
INSERT INTO `tbl_cabang` VALUES (332305, ' Kaloran');
INSERT INTO `tbl_cabang` VALUES (332306, ' Kandangan');
INSERT INTO `tbl_cabang` VALUES (332307, ' Kedu');
INSERT INTO `tbl_cabang` VALUES (332308, ' Parakan');
INSERT INTO `tbl_cabang` VALUES (332309, ' Ngadirejo');
INSERT INTO `tbl_cabang` VALUES (332310, ' Jumo');
INSERT INTO `tbl_cabang` VALUES (332311, ' Tretep');
INSERT INTO `tbl_cabang` VALUES (332312, ' Candiroto');
INSERT INTO `tbl_cabang` VALUES (332313, ' Kranggan');
INSERT INTO `tbl_cabang` VALUES (332314, ' Tlogomulyo');
INSERT INTO `tbl_cabang` VALUES (332315, ' Selopampang');
INSERT INTO `tbl_cabang` VALUES (332316, ' Bansari');
INSERT INTO `tbl_cabang` VALUES (332317, ' Kledung');
INSERT INTO `tbl_cabang` VALUES (332318, ' Bejen');
INSERT INTO `tbl_cabang` VALUES (332319, ' Wonoboyo');
INSERT INTO `tbl_cabang` VALUES (332320, ' Gemawang');
INSERT INTO `tbl_cabang` VALUES (332401, ' Plantungan');
INSERT INTO `tbl_cabang` VALUES (332402, ' Pageruyung');
INSERT INTO `tbl_cabang` VALUES (332403, ' Sukorejo');
INSERT INTO `tbl_cabang` VALUES (332404, ' Patean');
INSERT INTO `tbl_cabang` VALUES (332405, ' Singorojo');
INSERT INTO `tbl_cabang` VALUES (332406, ' Limbangan');
INSERT INTO `tbl_cabang` VALUES (332407, ' Boja');
INSERT INTO `tbl_cabang` VALUES (332408, ' Kaliwungu');
INSERT INTO `tbl_cabang` VALUES (332409, ' Brangsong');
INSERT INTO `tbl_cabang` VALUES (332410, ' Pegandon');
INSERT INTO `tbl_cabang` VALUES (332411, ' Gemuh');
INSERT INTO `tbl_cabang` VALUES (332412, ' Weleri');
INSERT INTO `tbl_cabang` VALUES (332413, ' Cepiring');
INSERT INTO `tbl_cabang` VALUES (332414, ' Patebon');
INSERT INTO `tbl_cabang` VALUES (332415, ' Kendal');
INSERT INTO `tbl_cabang` VALUES (332416, ' Rowosari');
INSERT INTO `tbl_cabang` VALUES (332417, ' Kangkung');
INSERT INTO `tbl_cabang` VALUES (332418, ' Ringinarum');
INSERT INTO `tbl_cabang` VALUES (332419, ' Ngampel');
INSERT INTO `tbl_cabang` VALUES (332420, ' Kaliwungu Selatan');
INSERT INTO `tbl_cabang` VALUES (332501, ' Wonotunggal');
INSERT INTO `tbl_cabang` VALUES (332502, ' Bandar');
INSERT INTO `tbl_cabang` VALUES (332503, ' Blado');
INSERT INTO `tbl_cabang` VALUES (332504, ' Reban');
INSERT INTO `tbl_cabang` VALUES (332505, ' Bawang');
INSERT INTO `tbl_cabang` VALUES (332506, ' Tersono');
INSERT INTO `tbl_cabang` VALUES (332507, ' Gringsing');
INSERT INTO `tbl_cabang` VALUES (332508, ' Limpung');
INSERT INTO `tbl_cabang` VALUES (332509, ' Subah');
INSERT INTO `tbl_cabang` VALUES (332510, ' Tulis');
INSERT INTO `tbl_cabang` VALUES (332511, ' Batang');
INSERT INTO `tbl_cabang` VALUES (332512, ' Warungasem');
INSERT INTO `tbl_cabang` VALUES (332513, ' Kandeman');
INSERT INTO `tbl_cabang` VALUES (332514, ' Pecalungan');
INSERT INTO `tbl_cabang` VALUES (332515, ' Banyuputih');
INSERT INTO `tbl_cabang` VALUES (332601, ' Kandangserang');
INSERT INTO `tbl_cabang` VALUES (332602, ' Paninggaran');
INSERT INTO `tbl_cabang` VALUES (332603, ' Lebakbarang');
INSERT INTO `tbl_cabang` VALUES (332604, ' Petungkriyono');
INSERT INTO `tbl_cabang` VALUES (332605, ' Talun');
INSERT INTO `tbl_cabang` VALUES (332606, ' Doro');
INSERT INTO `tbl_cabang` VALUES (332607, ' Karanganyar');
INSERT INTO `tbl_cabang` VALUES (332608, ' Kajen');
INSERT INTO `tbl_cabang` VALUES (332609, ' Kesesi');
INSERT INTO `tbl_cabang` VALUES (332610, ' Sragi');
INSERT INTO `tbl_cabang` VALUES (332611, ' Bojong');
INSERT INTO `tbl_cabang` VALUES (332612, ' Wonopringgo');
INSERT INTO `tbl_cabang` VALUES (332613, ' Kedungwuni');
INSERT INTO `tbl_cabang` VALUES (332614, ' Buaran');
INSERT INTO `tbl_cabang` VALUES (332615, ' Tirto');
INSERT INTO `tbl_cabang` VALUES (332616, ' Wiradesa');
INSERT INTO `tbl_cabang` VALUES (332617, ' Siwalan');
INSERT INTO `tbl_cabang` VALUES (332618, ' Karangdadap');
INSERT INTO `tbl_cabang` VALUES (332619, ' Wonokerto');
INSERT INTO `tbl_cabang` VALUES (332701, ' Moga');
INSERT INTO `tbl_cabang` VALUES (332702, ' Pulosari');
INSERT INTO `tbl_cabang` VALUES (332703, ' Belik');
INSERT INTO `tbl_cabang` VALUES (332704, ' Watukumpul');
INSERT INTO `tbl_cabang` VALUES (332705, ' Bodeh');
INSERT INTO `tbl_cabang` VALUES (332706, ' Bantarbolang');
INSERT INTO `tbl_cabang` VALUES (332707, ' Randudongkal');
INSERT INTO `tbl_cabang` VALUES (332708, ' Pemalang');
INSERT INTO `tbl_cabang` VALUES (332709, ' Taman');
INSERT INTO `tbl_cabang` VALUES (332710, ' Petarukan');
INSERT INTO `tbl_cabang` VALUES (332711, ' Ampelgading');
INSERT INTO `tbl_cabang` VALUES (332712, ' Comal');
INSERT INTO `tbl_cabang` VALUES (332713, ' Ulujami');
INSERT INTO `tbl_cabang` VALUES (332714, ' Warungpring');
INSERT INTO `tbl_cabang` VALUES (332801, ' Margasari');
INSERT INTO `tbl_cabang` VALUES (332802, ' Bumijawa');
INSERT INTO `tbl_cabang` VALUES (332803, ' Bojong');
INSERT INTO `tbl_cabang` VALUES (332804, ' Balapulang');
INSERT INTO `tbl_cabang` VALUES (332805, ' Pagerbarang');
INSERT INTO `tbl_cabang` VALUES (332806, ' Lebaksiu');
INSERT INTO `tbl_cabang` VALUES (332807, ' Jatinegara');
INSERT INTO `tbl_cabang` VALUES (332808, ' Kedungbanteng');
INSERT INTO `tbl_cabang` VALUES (332809, ' Pangkah');
INSERT INTO `tbl_cabang` VALUES (332810, ' Slawi');
INSERT INTO `tbl_cabang` VALUES (332811, ' Adiwerna');
INSERT INTO `tbl_cabang` VALUES (332812, ' Talang');
INSERT INTO `tbl_cabang` VALUES (332813, ' Dukuhturi');
INSERT INTO `tbl_cabang` VALUES (332814, ' Tarub');
INSERT INTO `tbl_cabang` VALUES (332815, ' Kramat');
INSERT INTO `tbl_cabang` VALUES (332816, ' Suradadi');
INSERT INTO `tbl_cabang` VALUES (332817, ' Warureja');
INSERT INTO `tbl_cabang` VALUES (332818, ' Dukuhwaru');
INSERT INTO `tbl_cabang` VALUES (332901, ' Salem');
INSERT INTO `tbl_cabang` VALUES (332902, ' Bantarkawung');
INSERT INTO `tbl_cabang` VALUES (332903, ' Bumiayu');
INSERT INTO `tbl_cabang` VALUES (332904, ' Paguyangan');
INSERT INTO `tbl_cabang` VALUES (332905, ' Sirampog');
INSERT INTO `tbl_cabang` VALUES (332906, ' Tonjong');
INSERT INTO `tbl_cabang` VALUES (332907, ' Jatibarang');
INSERT INTO `tbl_cabang` VALUES (332908, ' Wanasari');
INSERT INTO `tbl_cabang` VALUES (332909, ' Brebes');
INSERT INTO `tbl_cabang` VALUES (332910, ' Songgom');
INSERT INTO `tbl_cabang` VALUES (332911, ' Kersana');
INSERT INTO `tbl_cabang` VALUES (332912, ' Losari');
INSERT INTO `tbl_cabang` VALUES (332913, ' Tanjung');
INSERT INTO `tbl_cabang` VALUES (332914, ' Bulakamba');
INSERT INTO `tbl_cabang` VALUES (332915, ' Larangan');
INSERT INTO `tbl_cabang` VALUES (332916, ' Ketanggungan');
INSERT INTO `tbl_cabang` VALUES (332917, ' Banjarharjo');
INSERT INTO `tbl_cabang` VALUES (337101, ' Magelang Selatan');
INSERT INTO `tbl_cabang` VALUES (337102, ' Magelang Utara');
INSERT INTO `tbl_cabang` VALUES (337103, ' Magelang Tengah');
INSERT INTO `tbl_cabang` VALUES (337201, ' Laweyan');
INSERT INTO `tbl_cabang` VALUES (337202, ' Serengan');
INSERT INTO `tbl_cabang` VALUES (337203, ' Pasar Kliwon');
INSERT INTO `tbl_cabang` VALUES (337204, ' Jebres');
INSERT INTO `tbl_cabang` VALUES (337205, ' Banjarsari');
INSERT INTO `tbl_cabang` VALUES (337301, ' Sidorejo');
INSERT INTO `tbl_cabang` VALUES (337302, ' Tingkir');
INSERT INTO `tbl_cabang` VALUES (337303, ' Argomulyo');
INSERT INTO `tbl_cabang` VALUES (337304, ' Sidomukti');
INSERT INTO `tbl_cabang` VALUES (337401, ' Semarang Tengah');
INSERT INTO `tbl_cabang` VALUES (337402, ' Semarang Utara');
INSERT INTO `tbl_cabang` VALUES (337403, ' Semarang Timur');
INSERT INTO `tbl_cabang` VALUES (337404, ' Gayamsari');
INSERT INTO `tbl_cabang` VALUES (337405, ' Genuk');
INSERT INTO `tbl_cabang` VALUES (337406, ' Pedurungan');
INSERT INTO `tbl_cabang` VALUES (337407, ' Semarang Selatan');
INSERT INTO `tbl_cabang` VALUES (337408, ' Candisari');
INSERT INTO `tbl_cabang` VALUES (337409, ' Gajahmungkur');
INSERT INTO `tbl_cabang` VALUES (337410, ' Tembalang');
INSERT INTO `tbl_cabang` VALUES (337411, ' Banyumanik');
INSERT INTO `tbl_cabang` VALUES (337412, ' Gunungpati');
INSERT INTO `tbl_cabang` VALUES (337413, ' Semarang Barat');
INSERT INTO `tbl_cabang` VALUES (337414, ' Mijen');
INSERT INTO `tbl_cabang` VALUES (337415, ' Ngaliyan');
INSERT INTO `tbl_cabang` VALUES (337416, ' Tugu');
INSERT INTO `tbl_cabang` VALUES (337501, ' Pekalongan Barat');
INSERT INTO `tbl_cabang` VALUES (337502, ' Pekalongan Timur');
INSERT INTO `tbl_cabang` VALUES (337503, ' Pekalongan Utara');
INSERT INTO `tbl_cabang` VALUES (337504, ' Pekalongan Selatan');
INSERT INTO `tbl_cabang` VALUES (337601, ' Tegal Barat');
INSERT INTO `tbl_cabang` VALUES (337602, ' Tegal Timur');
INSERT INTO `tbl_cabang` VALUES (337603, ' Tegal Selatan');
INSERT INTO `tbl_cabang` VALUES (337604, ' Margadana');

-- ----------------------------
-- Table structure for tbl_daerah
-- ----------------------------
DROP TABLE IF EXISTS `tbl_daerah`;
CREATE TABLE `tbl_daerah`  (
  `kode_daerah` int(4) NOT NULL,
  `nama_daerah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`kode_daerah`) USING BTREE,
  UNIQUE INDEX `unix`(`kode_daerah`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_daerah
-- ----------------------------
INSERT INTO `tbl_daerah` VALUES (3301, 'Kabupaten Cilacap');
INSERT INTO `tbl_daerah` VALUES (3302, 'Kabupaten Banyumas');
INSERT INTO `tbl_daerah` VALUES (3303, 'Kabupaten Purbalingga');
INSERT INTO `tbl_daerah` VALUES (3304, 'Kabupaten Banjarnegara');
INSERT INTO `tbl_daerah` VALUES (3305, 'Kabupaten Kebumen');
INSERT INTO `tbl_daerah` VALUES (3306, 'Kabupaten Purworejo');
INSERT INTO `tbl_daerah` VALUES (3307, 'Kabupaten Wonosobo');
INSERT INTO `tbl_daerah` VALUES (3308, 'Kabupaten Magelang');
INSERT INTO `tbl_daerah` VALUES (3309, 'Kabupaten Boyolali');
INSERT INTO `tbl_daerah` VALUES (3310, 'Kabupaten Klaten');
INSERT INTO `tbl_daerah` VALUES (3311, 'Kabupaten Sukoharjo');
INSERT INTO `tbl_daerah` VALUES (3312, 'Kabupaten Wonogiri');
INSERT INTO `tbl_daerah` VALUES (3313, 'Kabupaten Karanganyar');
INSERT INTO `tbl_daerah` VALUES (3314, 'Kabupaten Sragen');
INSERT INTO `tbl_daerah` VALUES (3315, 'Kabupaten Grobogan');
INSERT INTO `tbl_daerah` VALUES (3316, 'Kabupaten Blora');
INSERT INTO `tbl_daerah` VALUES (3317, 'Kabupaten Rembang');
INSERT INTO `tbl_daerah` VALUES (3318, 'Kabupaten Pati');
INSERT INTO `tbl_daerah` VALUES (3319, 'Kabupaten Kudus');
INSERT INTO `tbl_daerah` VALUES (3320, 'Kabupaten Jepara');
INSERT INTO `tbl_daerah` VALUES (3321, 'Kabupaten Demak');
INSERT INTO `tbl_daerah` VALUES (3322, 'Kabupaten Semarang');
INSERT INTO `tbl_daerah` VALUES (3323, 'Kabupaten Temanggung');
INSERT INTO `tbl_daerah` VALUES (3324, 'Kabupaten Kendal');
INSERT INTO `tbl_daerah` VALUES (3325, 'Kabupaten Batang');
INSERT INTO `tbl_daerah` VALUES (3326, 'Kabupaten Pekalongan');
INSERT INTO `tbl_daerah` VALUES (3327, 'Kabupaten Pemalang');
INSERT INTO `tbl_daerah` VALUES (3328, 'Kabupaten Tegal');
INSERT INTO `tbl_daerah` VALUES (3329, 'Kabupaten Brebes');
INSERT INTO `tbl_daerah` VALUES (3371, 'Kota Magelang');
INSERT INTO `tbl_daerah` VALUES (3372, 'Kota Surakarta');
INSERT INTO `tbl_daerah` VALUES (3373, 'Kota Salatiga');
INSERT INTO `tbl_daerah` VALUES (3374, 'Kota Semarang');
INSERT INTO `tbl_daerah` VALUES (3375, 'Kota Pekalongan');
INSERT INTO `tbl_daerah` VALUES (3376, 'Kota Tegal');

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_ktp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto_nbm` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pas_foto` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `surat_mandat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sk_pimpinan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `swo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `swp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  `pendaftar_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_nilai_pendaftar1_idx`(`pendaftar_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_UNIQUE`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (41, 'Muzain', 'zain@gmail.com', '1234', 'peserta');
INSERT INTO `users` VALUES (42, 'Ahmad', 'ahmad@gmail.com', '1234', 'peserta');
INSERT INTO `users` VALUES (44, 'Rosid', 'rosidi@gmail.com', '1234', 'peserta');
INSERT INTO `users` VALUES (45, 'Mandra', 'marga@gmail.com', '1234', 'peserta');
INSERT INTO `users` VALUES (46, 'Hana', 'hana', '1234', 'peserta');
INSERT INTO `users` VALUES (47, 'administrator', 'admin', 'password', 'admin');
INSERT INTO `users` VALUES (48, 'Marsudi', 'marsudi@gmail.com', '1234', 'peserta');

SET FOREIGN_KEY_CHECKS = 1;
