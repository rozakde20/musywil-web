<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aplikasi Pendaftaran Musywil Pemuda Muhammadiyah Jateng">
    <meta name="author" content="muhammad-muza'in">

    <title>Daftar - Musywil PWM Jateng</title>
    <link rel="icon" href="assets/img/logo-pm.png" type="image/png">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- datepicker -->
    <link href="assets/vendor/datepicker/css/datepicker-bs4.min.css" rel="stylesheet" />


    <!-- Style untuk mengatur logo -->
    <style>
        .img-logo {
            max-height: 160px;
            margin-bottom: 20px;
        }
    </style>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-md-7">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 font-weight-bold">Registrasi Peserta Musyawarah Wilayah</h1>
                                        
                                    </div>
                                    <form class="user" method="post" enctype="multipart/form-data" action="registrasi_control.php" id="form-registrasi">
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="pimpinan">Pimpinan</label>
                                            <select id="pimpinan" name="pimpinan" class="form-control text-gray-900" placeholder="Pilih Nama Pimpinan" required>
                                                <option value="PWPM JATENG">PWPM JATENG</option>
                                                <option value="PDPM">PDPM</option>
                                                <option value="PCPM">PCPM</option>
                                            </select>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="nama">Nama</label>
                                            <input type="text" name="nama" class="form-control text-gray-900"
                                            id="nama" placeholder="Masukkan nama sesuai KTP" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="jabatan">Jabatan</label>
                                            <input type="text" name="jabatan" class="form-control text-gray-900"
                                                id="jabatan" placeholder="Masukkan Jabatan" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                            <input type="tel" name="nik" class="form-control text-gray-900"
                                                id="nik" placeholder="Nomor Induk Kependudukan" pattern="[0-9]{16}" title="Input harus terdiri dari 16 angka" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label id="label-daerah" style="display: none;">Nama Pimpinan Daerah</label>
                                            <input type="text" name="nama-daerah" class="form-control text-gray-900"
                                                id="nama-daerah" placeholder="Nama Pimpinan Daerah" style="display: none;">
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label id="label-cabang" style="display: none;">Nama Pimpinan Cabang</label>
                                            <input type="text" name="nama-cabang" class="form-control text-gray-900"
                                                id="nama-cabang" placeholder="Nama Pimpinan Cabang" style="display: none;">
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="nbm">NBM</label>
                                            <input type="text" name="nbm" class="form-control text-gray-900"
                                                id="nbm" placeholder="NBM">
                                        </div>
                                        <div class="form-group row text-gray-900 font-weight-bold">
                                            <div class="col-md-6">
                                                <label for="tempat-lahir">Tempat Lahir</label>
                                                <input type="text" class="form-control" id="tempat-lahir" placeholder="Tempat lahir" name="tempat-lahir">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="tanggal-lahir">Tanggal Lahir</label>
                                                <input type="text" class="form-control" id="tanggal-lahir" placeholder="tanggal-bulan-tahun"  name="tanggal-lahir">
                                            </div>
                                        </div>

                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="nomor">No. Whatsapp</label>
                                            <input type="tel" name="nomor" class="form-control text-gray-900"
                                                id="nomor" placeholder="No. Whatsapp" pattern="^(?:\+62|0)[8]{1}[1-9]{1}\d{7,11}$" title="Masukkan format yang benar diawali dengan +62 atu 0" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" class="form-control text-gray-900"
                                                id="email" placeholder="Email" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" class="form-control text-gray-900"
                                                id="password" placeholder="Buat password" required>
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="password">Ketik Ulang Password</label>
                                            <input type="password" name="confirmPassword" class="form-control text-gray-900"
                                                id="confirmPassword" placeholder="Ketik Ulang Password" required>
                                                <span id="confirmPasswordError" class="alert text-danger"></span>
                                        </div>
                                        <div class="form-group font-weight-bold">
                                            <button name="btn_registrasi" class="btn bg-gradient-primary btn-user btn-block text-gray-100">Daftar</button>
                                        </div>
                    
                                    </form>

                                    <!-- OPTION AGAR PIMPINAN MUNCUL NAMA CABANG-DAERAH -->
                                    <script>
                                    $(document).ready(function() {
                                        $("#pimpinan").change(function() {
                                            var selectedOrg = $(this).val();

                                            // Hide all fields
                                            $("#nama-daerah").hide();
                                            $("#label-daerah").hide();
                                            $("#nama-cabang").hide();
                                            $("#label-cabang").hide();

                                            // Show fields based on selected organization
                                            if (selectedOrg === "PDPM") {
                                                $("#nama-daerah").show();
                                                $("#label-daerah").show();
                                            } else if (selectedOrg === "PCPM") {
                                                $("#nama-cabang").show();
                                                $("#label-cabang").show();
                                            }
                                        });
                                    });
                                </script>

                                <!-- FORM VALIDATION UNTUK PASSWORD -->
                               



                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="login.php">Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="assets/vendor/datepicker/js/datepicker-full.min.js"></script>
    <script>
        const elem = document.querySelector('input[name="tanggal-lahir"]');
        const datepicker = new Datepicker(elem, {
            // options here
        });
            </script>






</body>

</html>