<?php

session_start();

include('koneksi.php');

// $base_url = "http://localhost:8080/musywil/";
$domain = $_SERVER['HTTP_HOST'];
$domain .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
$base_url = "localhost:8080/musywil/". $domain;
if (!empty($_SERVER['HTTPS'])) {
	$base_url = "https://" . $domain;
}


$uri_segment = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$ius=count($uri_segment)-2;//indek_level_uri_segment
// var_dump($uri_segment);

if(isset($_SESSION['status']) && $_SESSION['status'] == 'login') {
    // lanjut
    if($uri_segment[$ius] == $_SESSION['level']) {
        // lanjut
    } else {
        echo "Error: Forbidden !!!";
        echo "<br><br> <button type='button' onclick='history.back()'> Kembali </button>";
        die;
    }

} else {
    $_SESSION['login_error'] = "Silahkan Login untuk masuk kedalam sistem";
    
    header('location:'.home_base_url().'login.php');
}
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);
// function base_url($url){
//     GLOBAL $base_url;
//     return $base_url.'/'.$url;
// }
function home_base_url(){
    GLOBAL $base_url;
    return str_replace(explode("/",str_replace("http://","",str_replace("https://","",$base_url)))[(count(explode("/",str_replace("http://","",str_replace("https://","",$base_url))))-2)]."/","",$base_url);
}
function base_url($url){
    GLOBAL $base_url;
    return $base_url.$url;
}
function sesuaikan_tgl($tgl){//"27/10/1991"
    $date=explode("/",$tgl);
    $out=$date[2].'-'.$date[1].'-'.$date[0];
    return $out;
}
function nama_daerah($kode_daerah){
    if(strlen($kode_daerah)!=4){$out='-';}
    else{$out=query_row('select nama_daerah from tbl_daerah where kode_daerah="'.$kode_daerah.'"','nama_daerah');}
    return $out;
}
function nama_cabang($kode_cabang){
    if(strlen($kode_cabang)!=6){$out='-';}
    else{$out=query_row('select nama_cabang from tbl_cabang where kode_cabang="'.$kode_cabang.'"','nama_cabang');}
    return $out;
}
function query_row($sql,$field_select){
    GLOBAL $koneksi;
    
    $result = mysqli_query($koneksi,$sql);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    return $row[$field_select]; 
}
function import($field,$value,$f,$v){
    GLOBAL $koneksi;
    $sql_insert = "insert into pendaftar ($field) value (".implode(",",$value).");";
    $nbm=$value[6];
    $sql_user = "SELECT nbm FROM pendaftar where nbm = $nbm";
    $sql_update="UPDATE pendaftar set ";
    for($x=0;$x<count($f);$x++){
        if($x==(count($f)-1)){
            $sql_update.=$f[$x]."='".addslashes($v[$x])."' ";
        }else{
            $sql_update.=$f[$x]."='".addslashes($v[$x])."', ";
        }
    }
    $sql_update.="where nbm = $nbm ;";


    $result_user = mysqli_query($koneksi, $sql_user);

    if(mysqli_num_rows($result_user)> 0) {
        if(mysqli_query($koneksi, $sql_update)){
            return 2;
        }
        else{
            return 0;
        }
    }
    else{
        if(mysqli_query($koneksi, $sql_insert)){
            return 1;
        }
        else{
            return 0;
        }
    }

    // return implode('+',$value).'# #'.$sql_insert.'<br/>'.$sql_update.'<hr/>';//$sql_user.mysqli_num_rows($result_user);
}
?>