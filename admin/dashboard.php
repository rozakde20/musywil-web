
<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Admin</h1>
                    <div class="row">
                    <div class="col-md-6">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h4 font-weight-bold text-warning text-uppercase mb-1">BELUM TERVERIFIKASI
                                            </div>
                                            <div class="h5 mt-3 font-weight-bold">500 orang</div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col">
                                                    <div class="progress progress-sm mr-2">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users fa-2x text-gray-300 " style="font-size: 70px;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h4 font-weight-bold text-success text-uppercase mb-1">TERVERIFIKASI
                                                </div>
                                                <div class="h5 mt-3 font-weight-bold">500 orang</div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-users fa-2x text-gray-300 " style="font-size: 70px;"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <hr class="mt-3">
                    <h2 class="text-gray-900">DATA PENDAFTAR</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>NBM</td>
                                    <td>Jabatan</td>
                                    <td>Daerah</td>
                                    <td>Cabang</td>
                                    <td>Opsi</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Dr. Muhammad Muza'in, S.Kom, M.Kom</td>
                                    <td>331675554</td>
                                    <td>Ketua Bidang Komunikasi</td>
                                    <td>Semarang</td>
                                    <td>Tembalang</td>
                                    <td class="badge badge-success">Terverifikasi</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>