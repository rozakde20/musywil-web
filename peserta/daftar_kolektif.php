<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Pendaftar</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <b>Perhatian!</b> Silahkan unduh template pengisian data ini pada alamat berikut: <a target="_blank" href="https://docs.google.com/spreadsheets/d/19Sj8Jj36KH-S5BamIgUowp3SgfVl--Qc/edit" title="Klik untuk download template">Template</a>. Pastikan Saudara mengisi template tersebut dengan memperhatikan catatan pada kolom pengisian.
                            </div>
                            <a href="import_data_utusan.php" class="btn btn-primary alert"><i class="fas fa-upload"></i> Daftar Kolektif</a>
                            
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">Data Utusan Daerah/Cabang</div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No</td>
                                            <td>Nama</td>
                                            <td>NBM</td>
                                            <td>Jabatan</td>
                                            <td>Daerah</td>
                                            <td>Cabang</td>
                                            <td>Whatsapp</td>
                                            <td>Email</td>
                                            <td>Aksi</td>
                                        </tr>
                                <?php
                                $sql_user = "SELECT * FROM pendaftar";
                                $result_user = mysqli_query($koneksi, $sql_user);
                                if(mysqli_num_rows($result_user)> 0) {
                                    $no=1;
                                    while($data_user = mysqli_fetch_array($result_user)){
                                        echo "<tr>
                                        <td>".$no.".</td>
                                        <td>".$data_user['nama']."</td>
                                        <td>".$data_user['nbm']."</td>
                                        <td>".$data_user['jabatan']."</td>
                                        <td>".nama_daerah($data_user['daerah'])."</td>
                                        <td>".nama_cabang($data_user['cabang'])."</td>
                                        <td>".$data_user['nomor']."</td>
                                        <td>".$data_user['email']."</td>
                                        <td>-</td>
                                        </tr>";
                                        $no++;
                                    }
                                }
                                else{
                                ?>
                                        <tr>
                                            <td colspan="9">- empty -</td>
                                        </tr>
                                <?php
                                }
                                ?>
                                    </table>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>
<script>
$(document).ready(function(){
    $('.datatable-table').DataTable();
});
</script>