<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Pendaftar</h1>
                    <div class="row">
                    <div class="col-md-8">
                        
                        <!-- Biodata -->
                        <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Lengkapi Formulir Data Diri</h6>
                                </div>
                                <div class="card-body">
                                <form class="user">
                                        <div class="form-group">
                                            <input type="text" name="nama" class="form-control form-control-user"
                                                id="nama"
                                                placeholder="Masukkan nama sesuai KTP">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="jabatan" class="form-control form-control-user"
                                                id="jabatan" placeholder="Masukkan Jabatan">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="nama-daerah" class="form-control form-control-user"
                                                id="nama-daerah" placeholder="Nama Daerah">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="nik" class="form-control form-control-user"
                                                id="nik" placeholder="Nomor Induk Kependudukan">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="nbm" class="form-control form-control-user"
                                                id="nbm" placeholder="NBM">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="tempat-lahir" class="form-control form-control-user"
                                                id="tempat-lahir" placeholder="Tempat lahir">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="tanggal-lahir" class="form-control form-control-user"
                                                id="tanggal-lahir" placeholder="Tanggal lahir">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="nomor" class="form-control form-control-user"
                                                id="nomor" placeholder="Nomor Whatsapp">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control form-control-user"
                                                id="email" placeholder="Alamat Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="ukuran_kaos">Ukuran Kaos</label>
                                            <select class="form-control" id="ukuran_kaos" name="ukuran_kaos" required>
                                                <option value="S">S</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                                <option value="XXL">XXL</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload Foto KTP</label><br>
                                            <input type="file" name="ktp" placeholder="Upload Foto KTP"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload foto NBM</label><br>
                                            <input type="file" name="nbm" placeholder="Upload Foto NBM"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload Pas foto</label><br>
                                            <input type="file" name="pas-foto" placeholder="Upload Pas Foto"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload Surat Mandat</label><br>
                                            <input type="file" name="pas-foto" placeholder="Upload surat mandat"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload SK Pimpinan</label><br>
                                            <input type="file" name="pas-foto" placeholder="Upload SK Pimpinan"/>
                                        </div>
                                        <a href="index.html" class="btn btn-primary mb-5">
                                            Simpan
                                        </a>
                                        <a href="dashboard.php" class="btn btn-danger mb-5">Kembali</a>
                    
                                    </form>
                                </div>
                            </div>

                        </div>

                    <div class="col-md-4">
                        <img src="../assets/img/avatar.png" alt="foto-profil" class="img-fluid">
                        <input type="file" name="profil" class="form-control mt-2">
                    </div>
                        




                        </div>

                        

                        
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>