<?php include('../config/auto_load.php') ?>
<?php include('dashboard_control.php') ?>
<?php include('../template/header.php') ?>
               <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Pendaftar</h1>
                    <div class="row">

                    <?php if(isset($status) && $status == 1) { ?>
                            <!-- DIPROSES -->
                        <div class="col-md-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>
                                </div>
                                <!-- DIPROSES PENDAFTARAN -->
                                <div class="card-body">
                                    <div class="card text-center">
                                        <h5 class="card-title mb-3 mt-3">Proses Pendaftaran</h5>
                                        <div class="col-auto">
                                            <i class="fas fa-spinner text-warning" style="font-size: 70px;"></i>
                                        </div>
                                        <p class="card-text">Pendaftaran anda sedang diproses</p>
                                        <span class="badge badge-warning">Pengumuman pada tanggal 10 Agustus</span>
                                    </div>               
                                </div>
               
                                </div>

                                <div class="card-footer">
                                    <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>
                                </div>
                            </div>
                            <?php } elseif (isset($status) && $status == 2) { ?>

                            <!-- LOLOS -->
                            <div class="col-md-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>
                                </div>
                                <!-- DIPROSES PENDAFTARAN -->
                                <div class="card-body">
                                    <div class="card text-center">
                                        <h5 class="card-title mb-3 mt-3">LOLOS</h5>
                                        <div class="col-auto">
                                            <i class="far fa-check-circle text-success" style="font-size: 70px;"></i>
                                        </div>
                                        <p class="card-text">Selamat Anda Lolos Pendaftaran</p>
                                        <span class="badge bg-gradient-primary">Pengumuman pada tanggal 10 Agustus</span>
                                    </div>               
                                </div>
               
                                </div>

                                <div class="card-footer">
                                    <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>
                                </div>
                            </div>
                            <?php } elseif(isset($status) && $status==0) {?>

                            <!-- GAGAL -->
                            <div class="col-md-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>
                                </div>
                                <!-- DIPROSES PENDAFTARAN -->
                                <div class="card-body">
                                    <div class="card text-center">
                                        <h5 class="card-title mb-3 mt-3">MENUNGGU PEMBAYARAN</h5>
                                        <div class="col-auto">
                                            <i class="fa fa-file-invoice text-danger" style="font-size: 70px;"></i>
                                        </div>
                                        <p class="card-text">SILAHKAN SELESAIKAN REGISTRASI DI HALAMAN PEMBAYARAN</p>
                                        <span class="badge badge-danger">Pengumuman pada tanggal 10 Agustus</span>
                                    </div>               
                                </div>
               
                                </div>

                                <div class="card-footer">
                                    <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>
                                </div>
                            </div>
                            <?php } ?>



                            <!-- INFORMASI -->
                            <div class="col-md-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-danger">Catatan Penting</h6>
                                </div>
                                <div class="card-body">
                                    <p class="text-danger">
                                        Silahkan untuk mengupload surat mandat kolektif dan surat keputusan pimpinan.
                                        Kemudian melakukan pendaftaran secara kolektif di menu daftar kolektif.
                                        Pendaftaran anda akan diverifikasi terlebih dahulu oleh admin. Status akan berubah menjadi
                                        lolos apabila persyaratan administrasi dan keuangan terverifikasi. Terima Kasih.
                                    </p>
                                    
                                </div>
                            </div>

                            </div>
                        </div>

                    
                    <!-- ROW DIV UNTUK PROFIL DAN FORMULIR -->
                    <div class="row">
                    <?php if(!isset($status)) { ?>
                    <div class="col-md-6">
                       <!-- Biodata -->
                        <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Silahkan isi link Google Drive</h6>
                                </div>
                                <!-- ALERT -->
                                <?php 
                                        
                                        if(isset($_SESSION['error_upload'])) { ?>

                                        <div class="alert alert-danger">
                                        <?= $_SESSION['error_upload'] ?>
                                        </div>
                    
                                        <?php } ?>
                                <div class="card-body">
                                <form class="user" method="post" enctype="multipart/form-data" action="<?= $base_url ?>/peserta/dashboard.php">
                                        <div class="form-group">
                                            <label for="">Upload Surat Mandat</label><br>
                                            <input type="textbox" name="surat_mandat" placeholder="Upload surat mandat" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Upload SK Pimpinan</label><br>
                                            <input type="textbox" name="sk_pimpinan" placeholder="Upload SK Pimpinan"  required/>
                                        </div>
                                        <button class="btn btn-primary" id="btn_simpan" name="btn_simpan">
                                            Simpan </button>
                                        </button>
                    
                                    </form>
                                </div>
                            </div>
    
                        </div>

                        <?php } ?>

                        <!-- FORM PEMBAYARAN -->
                        <!-- agar ditampilkan formnya setelah file diupload -->
                        <?php if(isset($status) && $status == 0) { ?>

                        <div class="col-md-6">                                
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-danger">Kewajiban Pembayaran</h6>
                                </div>
                                <div class="card-body">
                                    <p class="text-primary">
                                        Peserta diwajibkan membayar registrasi berupa Sumbangan Wajib Organisasi (SWO)
                                        dan Sumbangan Wajib Peserta (SWP) dengan nominal yang telah ditentukan yaitu:
                                        <br>
                                        <br> SWO : Rp. 500.000,00-
                                        <br> SWP : Rp. 250.000,00-
                                        <br>
                                        <br> Pembayaran Melalui Rekening Bank BSI Atas Nama PM Jateng (005)5043332222
                                        <br> Setelah melakukan pembayaran, upload bukti di form di bawah ini.
                                    </p>
                                    <p class="text-danger">
                                        Hati-hati! Panitia hanya menyediakan 1 Rekening pembayaran, jika ada rekening lain 
                                        yang mengatasnamakan panitia musywil PM jateng, harap waspada penipuan! 
                                    </p>
                                </div>
                                <div class="p-5">
                                    <form action="" class="user" method="post" enctype="multipart/form-data">
                                        <label for="" class="form-group text-gray-900">Sumbangan Wajib Organisasi (SWO)</label>
                                        <input type="text" name="swo" class="form-control text-gray-900" readonly placeholder="Rp. 500.000,00">
                                        <input type="file" name="swo" id="swo" class="form-control">

                                        <label for="" class="form-group text-gray-900 mt-4">Sumbangan Wajib Peserta (SWP)</label>
                                        <input type="text" name="swp" class="form-control text-gray-900" readonly placeholder="Rp. 250.000,00">
                                        <input type="file" name="swp" id="swp" class="form-control">

                                        <div class="text-right"> 
                                            <button class="btn btn-primary mt-3" name="btn_bayar" id="btn_bayar">
                                                UPLOAD
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            </div>

                            <?php } ?>

                        <!-- profil -->
                        <div class="col-md-6">
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Profil</h6>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                    
                                        <img src="../assets/img/avatar.png"  class="img-fluid rounded-circle" style="width: 200px">
                                        <h5 class="text-center card-title">Edi Siswanto</h5>
                                        <div class="text-center">
                                            <a href="data-diri.php" class="btn btn-warning">Edit Profil</a>
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <h6>Tempat, Tanggal Lahir</h6>
                                                <small>Banyumas, 20 Februari 1991</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Jabatan</h6>
                                                <small>Sekretaris</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>NBM</h6>
                                                <small>12123444544</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Asal daerah</h6>
                                                <small>Banyumas</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Asal Cabang</h6>
                                                <small>Banyuurip</small>
                                            </li>
                                        </ul>
    
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

 <?php include('../template/footer.php') ?>