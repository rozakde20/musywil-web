<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Form Import Data Utusan</h1>
                    <div class="row unggah">
                        <div class="col-md-12">
                            <!-- Import -->
                            <div class="card md-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Upload Data Template (XLSX)</h6>
                                    </div>
                                    <div class="card-body">
                                        <form class="user">
                                            
                                            <div class="form-group">
                                                <label for="">Upload Data Kolektif sesuai template</label><br>
                                                <input type="file" name="filenya" class="file" placeholder="Upload Data"/>
                                            </div>
                                        
                                            <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Import</a>
                                            <a class="btn btn-danger mb-5" href="daftar_kolektif.php" title="klik untuk kembali">Kembali</a>
                                        </form>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    $.ajax({
                        url: '<?=base_url("proses_import_utusan.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>