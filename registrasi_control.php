<?php

include('config/koneksi.php');
session_start();

if(isset($_POST['btn_registrasi'])) {

    $pimpinan = $_POST['pimpinan'];
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $nik = $_POST['nik'];
    $namaDaerah = $_POST['nama-daerah'];
    $namaCabang = $_POST['nama-cabang'];
    $nbm = $_POST['nbm'];
    $tempat_lahir = $_POST['tempat-lahir'];
    $tanggal_lahir = date("Y-m-d", strtotime($_POST['tanggal-lahir']));
    $whatsapp = $_POST['nomor'];
    $email = $_POST['email'];
    $password = ($_POST['password']);
    $confirmPassword = ($_POST['confirmPassword']);

    if($password != $confirmPassword) {
        echo "Error: Password tidak sama";
        echo "<br><br> <button type='button' onclick='history.back();'> Kembali </button>";
        die;
    }

    $sql_user =mysqli_query($koneksi, "INSERT INTO users (nama, username, password, level) values ( '$nama','$email','$password', 'peserta')" );
   

    if($sql_user){
        $data_user = mysqli_query($koneksi, "SELECT LAST_INSERT_ID()");
        while($u = mysqli_fetch_array($data_user)){
            $user_id = $u[0];
        }

        $sql_pendaftar ="INSERT INTO pendaftar (pimpinan, nama, nik, nbm, jabatan, daerah, cabang, tmpt_lahir, tgl_lahir, email, nomor, users_id) values ('$pimpinan', '$nama', '$nik', '$nbm', '$jabatan', '$namaDaerah', '$namaCabang', '$tempat_lahir', '$tanggal_lahir', '$email', '$whatsapp', '$user_id')";
        $result_pendaftar = mysqli_query($koneksi, $sql_pendaftar);

        if($result_pendaftar){
            // jika berhasil
            $_SESSION['pesan_registrasi'] = "Registrasi Berhasil, Login Menggunakan Email dan Password anda!";

            header('location:login.php');

        } else {
            // jika query pendaftar gagal
            echo "Error insert pendaftar ". mysqli_error($koneksi);
            echo "<br><br> <button type='button' onclick='history.back();'> Kembali </button>";
            die;
        }
} else {
    // jika query users gagal
    echo "Error insert users: ". mysqli_error($koneksi);
    echo "<br><br> <button type='button' onclick='history.back();'> Kembali </button>";
    die;
}


    } 
 else {
    header('location:registrasi.php');
}

